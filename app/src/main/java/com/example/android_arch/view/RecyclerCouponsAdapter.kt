package com.example.android_arch.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.android_arch.BR
import com.example.android_arch.model.Coupon
import com.example.android_arch.viewmodel.CouponViewModel

class RecyclerCouponsAdapter(val couponViewModel: CouponViewModel, val resource: Int) :
    RecyclerView.Adapter<RecyclerCouponsAdapter.CardCouponHolder>() {

    private var coupons: List<Coupon>? = null

    fun setCouponsList(couponsList: List<Coupon>) {
        this.coupons = couponsList
    }

    override fun getItemCount(): Int = coupons?.size ?: 0

    override fun onBindViewHolder(holder: CardCouponHolder, position: Int) {
        holder.setDataCard(couponViewModel, position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardCouponHolder {
        val layoutInflater: LayoutInflater = LayoutInflater.from(parent.context)
        val binding: ViewDataBinding =
            DataBindingUtil.inflate(layoutInflater, viewType, parent, false)
        return CardCouponHolder(binding)
    }

    override fun getItemViewType(position: Int): Int = getLayoutIdByPosition(position)

    private fun getLayoutIdByPosition(position: Int): Int = resource

    class CardCouponHolder(binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

        private var binding: ViewDataBinding? = null

        init {
            this.binding = binding
        }

        fun setDataCard(couponViewModel: CouponViewModel, position: Int) {
            binding?.setVariable(BR.model, couponViewModel)
            binding?.setVariable(BR.position, position)
            binding?.executePendingBindings()
        }
    }
}