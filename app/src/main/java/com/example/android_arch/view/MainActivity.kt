package com.example.android_arch.view

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.android_arch.R
import com.example.android_arch.databinding.ActivityMainBinding
import com.example.android_arch.viewmodel.CouponViewModel

class MainActivity : AppCompatActivity() {

    lateinit var couponsViewModel: CouponViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val activityMainBinding: ActivityMainBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_main)
        couponsViewModel = ViewModelProviders.of(this).get(CouponViewModel::class.java)
        couponsViewModel.callCoupons()
        couponsViewModel.getCoupons().observe(this, Observer { itList ->
            couponsViewModel.setCouponsInRecyclerAdapter(itList)
            Log.w(javaClass.name, itList.toString())
        })
        activityMainBinding.model = couponsViewModel
        supportActionBar?.hide()
    }
}