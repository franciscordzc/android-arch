package com.example.android_arch.model

import androidx.lifecycle.MutableLiveData

interface CouponRepository {
    fun callCouponsAPI()
    fun getCouponsAPI(): MutableLiveData<List<Coupon>>
}