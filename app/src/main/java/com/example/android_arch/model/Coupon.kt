package com.example.android_arch.model

import com.google.gson.JsonObject
import java.io.Serializable
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class Coupon(couponJson: JsonObject) : Serializable {
    lateinit var id: String
    lateinit var imageURL: String
    lateinit var title: String
    lateinit var descriptionShort: String
    lateinit var category: String
    lateinit var description: String
    lateinit var offer: String
    lateinit var webSite: String
    lateinit var endDate: String
    lateinit var url: String

    companion object {
        private const val ID = "lmd_id"
        private const val IMAGE_URL = "image_url"
        private const val TITLE = "title"
        private const val DESCRIPTION_SHORT = "offer_text"
        private const val CATEGORY = "categories"
        private const val DESCRIPTION = "description"
        private const val OFFER = "offer"
        private const val WEBSITE = "store"
        private const val END_DATE = "end_date"
        private const val URL = "url"
    }

    init {
        try {
            id = couponJson[ID].asString
            imageURL = couponJson[IMAGE_URL].asString
            title = couponJson[TITLE].asString
            descriptionShort = chunkWords(couponJson[DESCRIPTION_SHORT].asString, ' ', 5)
            category = chunkWords(couponJson[CATEGORY].asString, ',', 1)
            description = couponJson[DESCRIPTION].asString
            offer = couponJson[OFFER].asString
            webSite = couponJson[WEBSITE].asString
            endDate = getFormatDate(couponJson[END_DATE].asString)
            url = couponJson[URL].asString
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun getFormatDate(dateCoupon: String): String {
        val format = SimpleDateFormat("yyyy-MM-dd")
        val dateFormat = SimpleDateFormat("dd MMMM yyyy")
        var dateParsed: String
        try {
            val parsedDateFormat = format.parse(dateCoupon)
            val cal = Calendar.getInstance()
            cal.time = parsedDateFormat
            dateParsed = dateFormat.format(cal.time)
        } catch (e: ParseException) {
            e.printStackTrace()
            dateParsed = ""
        }
        return dateParsed
    }

    private fun chunkWords(string: String, delimeter: Char, quantity: Int): String {
        val word = string.split(delimeter)
        var newString = ""
        for (i in 0..quantity) {
            newString += word[i] + " "
        }
        return newString
    }
}