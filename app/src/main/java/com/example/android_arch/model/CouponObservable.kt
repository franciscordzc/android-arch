package com.example.android_arch.model

import androidx.databinding.BaseObservable
import androidx.lifecycle.MutableLiveData

class CouponObservable : BaseObservable() {

    private var couponRepository: CouponRepository = CouponRepositoryImpl()

    fun callCoupons() {
        couponRepository.callCouponsAPI()
    }

    fun getCoupon(): MutableLiveData<List<Coupon>> = couponRepository.getCouponsAPI()

}