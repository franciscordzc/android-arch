package com.example.android_arch.viewmodel

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android_arch.R
import com.example.android_arch.model.Coupon
import com.example.android_arch.model.CouponObservable
import com.example.android_arch.view.RecyclerCouponsAdapter
import com.squareup.picasso.Picasso


class CouponViewModel : ViewModel() {

    private var couponObservable: CouponObservable = CouponObservable()
    private var recyclerCouponsAdapter: RecyclerCouponsAdapter? = null

    fun callCoupons() {
        couponObservable.callCoupons()
    }

    fun getCoupons(): MutableLiveData<List<Coupon>> = couponObservable.getCoupon()

    fun setCouponsInRecyclerAdapter(coupons: List<Coupon>) {
        recyclerCouponsAdapter?.setCouponsList(coupons)
        recyclerCouponsAdapter?.notifyDataSetChanged()
    }

    fun getRecyclerCouponsAdapter(): RecyclerCouponsAdapter =
        RecyclerCouponsAdapter(this, R.layout.card_coupon)

    fun getCouponAt(position: Int): Coupon? = couponObservable.getCoupon().value?.get(position)

    @BindingAdapter("imageUrl")
    fun loadImage(view: ImageView, imageUrl: String) {
        Picasso.get().load(imageUrl).resize(520, 520).centerCrop().into(view)
    }

}