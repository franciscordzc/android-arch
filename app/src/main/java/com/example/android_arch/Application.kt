package com.example.android_arch

import android.app.Application
import android.util.Log

class Application : Application() {
    override fun onCreate() {
        super.onCreate()
        Log.i(javaClass.name, "onCreateApp")
    }
}
